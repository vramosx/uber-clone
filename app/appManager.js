//import liraries
import React, { Component } from 'react';
import { View, Text, StyleSheet } from 'react-native';
import Splash from './views/splash';
import Map from './views/map';

// create a component
class AppManager extends Component {

    constructor()
    {
        super();
    }

    render() {
        return (
           <Splash />
        );
    }
}

//make this component available to the app
export default AppManager;
