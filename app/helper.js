let images = {
    uberLogo: require('./assets/images/uberIcon.jpg'),
    splashBg: require('./assets/images/splashBg.png'),
    iconHamburguer: require('./assets/images/iconHamburger.png'),
    iconArrowLeft: require('./assets/images/iconArrowLeft.png'),
    iconHome: require('./assets/images/iconHome.png'),
    iconRecent: require('./assets/images/iconRecent.png'),
    bottomBg: require('./assets/images/bottomGradientOverlay.png'),
}

export { images }