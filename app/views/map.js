//import liraries
import React, { Component } from 'react';
import { MapView } from 'expo';
import { images } from '../helper';
import * as Animatable from 'react-native-animatable';
import { 
    View, 
    Text, 
    StyleSheet, 
    Image,
    TouchableOpacity,
    Dimensions,
    Platform
} from 'react-native';

const { width, height } = Dimensions.get('window');

// create a component
class Map extends Component {

    componentDidMount()
    {
        const { map, 
                searchHeader, 
                ballContainer1, 
                recentBall1, 
                recentText1,
                ballContainer2, 
                recentBall2, 
                recentText2,
                ballContainer3, 
                recentBall3, 
                recentText3 
            } = this.refs;

        

        searchHeader.transitionTo({
            transform: [ { scale: 1 } ],
            top: height * 0.17,
            width: width * 0.85,
            height: 55
        });

        map.transitionTo({
            opacity: 0.8
        }, 500, 'linear')

        ballContainer1.transitionTo({
            left: 30
        }, 200, 'linear');

        ballContainer2.transitionTo({
            left: (width/2) - 40
        }, 300, 'linear');

        ballContainer3.transitionTo({
            right: 30
        }, 400, 'linear')
        .then(function() {
            recentBall1.transitionTo({
                width: 60,
                height: 60,
                borderRadius: 30,
                backgroundColor: '#FFF'
            }, 200).then(function() {
                recentText1.transitionTo({
                    opacity: 1
                }, 100, 'linear');
            });

            recentBall2.transitionTo({
                width: 60,
                height: 60,
                borderRadius: 30,
                backgroundColor: '#FFF'
            }, 400).then(function() {
                recentText2.transitionTo({
                    opacity: 1
                }, 100, 'linear');
            })

            recentBall3.transitionTo({
                width: 60,
                height: 60,
                borderRadius: 30,
                backgroundColor: '#FFF'
            }, 600).then(function() {
                recentText3.transitionTo({
                    opacity: 1
                }, 100, 'linear');
            });
        });
    }

    render() {
        return (
            <View style={styles.container}>
                <Animatable.View ref="map" style={styles.mapContainer}>
                    <MapView
                        style={{ flex: 1 }}
                        initialRegion={{
                            latitude: -23.6299936,
                            longitude: -46.6367464,
                            latitudeDelta: 0.0922,
                            longitudeDelta: 0.0421,
                        }}
                    >
                        <MapView.Marker style={{ zIndex: 10000 }} coordinate={{
                            latitude: -23.6299936,
                            longitude: -46.6367464,
                        }}>
                            <View style={styles.marker}/>
                        </MapView.Marker>
                    </MapView>
                </Animatable.View>
                <TouchableOpacity style={styles.buttonHamburguer}>
                    <Image style={styles.iconHamburguer} source={images.iconHamburguer} />
                    <Image style={styles.iconArrowLeft} source={images.iconArrowLeft} />
                </TouchableOpacity>
                <Animatable.View ref="searchHeader" style={styles.searchHeader}>
                    <View style={styles.box} />
                    <Text style={styles.headerText}>Para onde?</Text>
                </Animatable.View>

                <Image style={styles.bottomBg} source={images.bottomBg}/>

                <View style={styles.ballsContainer}>
                    <Animatable.View ref="ballContainer1" style={styles.ballContainer1}>
                        <Animatable.View ref="recentBall1" style={styles.recentBall}>
                            <Image style={styles.recentImage} source={images.iconRecent} />
                        </Animatable.View>
                        <Animatable.Text ref="recentText1" style={styles.recentText}>
                            Rua Soares de Avellar, 464
                        </Animatable.Text>
                    </Animatable.View>

                    <Animatable.View ref="ballContainer2" style={styles.ballContainer2}>
                        <Animatable.View ref="recentBall2" style={styles.recentBall}>
                            <Image style={styles.recentImage} source={images.iconRecent} />
                        </Animatable.View>
                        <Animatable.Text ref="recentText2" style={styles.recentText}>
                            Itaú CEIC - Eudoro
                        </Animatable.Text>
                    </Animatable.View>

                    <Animatable.View ref="ballContainer3" style={styles.ballContainer3}>
                        <Animatable.View ref="recentBall3" style={styles.recentBall}>
                            <Image style={styles.recentImage} source={images.iconRecent} />
                        </Animatable.View>
                        <Animatable.Text ref="recentText3" style={styles.recentText}>
                            Bye Xamarin
                        </Animatable.Text>
                    </Animatable.View>
                </View>
            </View>
        );
    }
}

// define your styles
const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'transparent',
    },
    mapContainer: {
        opacity: 0,
        width: '100%',
        height: '100%'
    },
    mapOverlay: {
        opacity: 0,
        width: '100%',
        height: '100%',
        position: 'absolute',
        backgroundColor: '#dfe3e5'
    },
    buttonHamburguer: {
        width: 20,
        height: 20,
        position: 'absolute',
        left: '5%',
        top: '5%'
    },
    iconHamburguer: {
        width: 20,
        height: 20,
        resizeMode: 'contain'
    },
    iconArrowLeft: {
        width: 20,
        height: 20,
        resizeMode: 'contain',
        position: 'absolute',
        opacity: 0
    },
    searchHeader: {
        width: width * 0.55,
        height: 25,
        top: -80,
        transform: [ { scale: 0.6 } ],
        backgroundColor: '#FFF',
        position: 'absolute',
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 3 },
        shadowOpacity: 0.3,
        shadowRadius: 5,
        flexDirection: 'row',
        alignItems: 'center',
        borderWidth: Platform.OS == 'ios' ? 0 : 1,
        borderColor: 'rgba(0,0,0,0.2)',
        borderBottomWidth: Platform.OS == 'ios' ? 0 : 4
    },
    headerText: {
        marginLeft: 10,
        color: '#676767',
        fontWeight: '100',
        fontSize: 19
    },
    box: {
        margin: 10,
        marginLeft: 25,
        backgroundColor: '#000',
        width: 6,
        height: 6
    },
    recentBall: {
        width: 6,
        height: 6,
        borderRadius: 3,
        borderWidth: Platform.OS == 'ios' ? 0 : 2,
        borderColor: 'rgba(0,0,0,0.2)',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#000',
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 3 },
        shadowOpacity: 0.15,
        shadowRadius: 5,
        overflow: 'hidden'
    },
    recentImage: {
        width: 20,
        height: 20,
        resizeMode: 'contain'
    },
    recentText: {
        width: 80,
        height: 60,
        textAlign: 'center',
        marginTop: 5,
        opacity: 0
    },
    ballsContainer: {
        width: '100%',
        bottom: '5%',
        height: 120,
        position: 'absolute',
        justifyContent: 'center',
    },
    ballContainer1: {
        alignItems: 'center',
        justifyContent: 'center',
        position: 'absolute',
        left: width*1.2   
    },
    ballContainer2: {
        alignItems: 'center',
        justifyContent: 'center',
        position: 'absolute',
        left: width*1.2
    },
    ballContainer3: {
        alignItems: 'center',
        justifyContent: 'center',
        position: 'absolute',
        right: -width*0.2
    },
    bottomBg: {
        width: '100%',
        height: '15%',
        position: 'absolute',
        bottom: 0,
        resizeMode: 'stretch'
    },
    markerContainer: {
        width: 100,
        borderRadius: 50,
        backgroundColor: '#c3d7e0',
        alignItems: 'center',
        justifyContent: 'center'
    },
    marker: {
        width: 20,
        borderRadius: 10,
        backgroundColor: '#3298ca'
    }
});

//make this component available to the app
export default Map;
