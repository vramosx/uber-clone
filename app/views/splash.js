//import liraries
import React, { Component } from 'react';
import * as Animatable from 'react-native-animatable';
import { images } from '../helper';
import Map from './map';
import 
{ 
    View, 
    Text, 
    StyleSheet,
    Image,
    Dimensions
} from 'react-native';

const { width, height } = Dimensions.get('window');

// create a component
class Splash extends Component {
    state = {
        loaded: false
    }

    componentDidMount()
    {
        var tempRef = this;
        const { transitionView, bg, bgImage } = this.refs;
        setTimeout(function(){
            transitionView.transitionTo({
                opacity: 1
            }, 200, 'linear').then(function(){
                    bg.transitionTo({
                        opacity: 0.4
                    }, 700, 'linear');
                    bgImage.transitionTo({
                        opacity: 1
                    }, 700, 'linear');
                    transitionView.transitionTo({
                        width: 1200,
                        height: 1200,
                        borderRadius: 600
                    }, 700, 'linear').then(function() {
                        tempRef.setState({ loaded: true });
                    });
                
            });
        }, 1000);
    }

    render() {
        return (
            <View style={styles.container}>
                <Animatable.View ref="bg" style={styles.bg}/>
                <Image style={styles.logo} source={images.uberLogo} />
                <Animatable.View ref="transitionView" style={styles.transitionView}>
                    <Animatable.Image ref="bgImage" style={styles.bgImage} source={images.splashBg} />
                </Animatable.View>
                <View style={styles.mapContainer}>
                    {
                        (this.state.loaded) ? 
                        <Map /> : null
                    }
                </View>
            </View>
        );
    }
}

// define your styles
const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#fff',
    },
    bg: {
        position: 'absolute',
        width: '100%',
        height: '100%',
        backgroundColor: '#000'
    },
    logo: {
        width: 180,
        height: 180
    },
    transitionView: {
        width: 70,
        height: 70,
        overflow: 'hidden',
        position: 'absolute',
        backgroundColor: '#FFF',
        borderRadius: 40,
        opacity: 0
    },
    bgImage: {
        resizeMode: 'cover',
        width: 1200,
        height: 1200,
        opacity: 0,
        borderRadius: 600
    },
    bgImage2: {
        resizeMode: 'cover',
        width: 1200,
        height: 1200,
    },
    mapContainer: {
        width: '100%',
        height: '100%',
        position: 'absolute'
    }
});

//make this component available to the app
export default Splash;
