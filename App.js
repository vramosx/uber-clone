import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import AppManager from './app/appManager';

export default class App extends React.Component {
  render() {
    return (
      <AppManager />
    );
  }
}